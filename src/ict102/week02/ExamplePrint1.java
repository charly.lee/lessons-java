package ict102.week02;

public class ExamplePrint1 {

    public static void main(String[] args) {
        System.out.println(12 / 5 + 8 / 4);
        System.out.println(3 * 4 + 15 / 2);
        System.out.println(-(1 + 2 * 3 + (1 + 2) * 3));

        System.out.println(42 % 5 + 16 % 3);
        System.out.println(2.5 * 2 + 17 / 4);
        System.out.println(4.5 / 3 / 2 + 1);
        System.out.println(2 + 6 + "cse 142");
        System.out.println("cse 142" + 2 + 6);

    }
}
