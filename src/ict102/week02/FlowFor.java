package ict102.week02;

public class FlowFor {
    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 4, 5 };

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
        }
        System.out.print(System.lineSeparator());

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < (i + 1); j++) {
                System.out.print(arr[j]);
            }
            System.out.print(System.lineSeparator());
        }
    }
}
