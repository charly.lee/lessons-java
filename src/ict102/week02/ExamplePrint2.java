package ict102.week02;

public class ExamplePrint2 {

    public static void main(String[] args) {
        System.out.println("Hearing in the distance\n\n");
        System.out.println("Two mandolins like creatures in the");
        System.out.println();
        System.out.println();
        System.out.println("dark");
        System.out.print(System.lineSeparator() + System.lineSeparator());
        System.out.println("Creating the agony of ecstasy.\n\n");
        System.out.println(" - George Barker\n\n");
    }
}
