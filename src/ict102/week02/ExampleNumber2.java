package ict102.week02;

public class ExampleNumber2 {
    public static void main(String [] args) {
        double pencil = 1.2d;
        double eraser = 2.0d;
        double compass = 5.0d;
        
        int howManyP = 11;
        int howManyE = 22;
        int howManyC = 33;
        
        double totalPrice = (pencil * howManyP) 
                    + (eraser * howManyE)
                    + (compass * howManyC);
       
        System.out.println("Total price: " + totalPrice);
    }
}
