package ict102.week02;

public class ExampleNumber1 {
    public static void main(String [] args) {
        float force =  172.5f;
        float area = 27.5f;
        float pressure = force / area;
        
        System.out.println("Pressure: " + pressure);
    }
}
