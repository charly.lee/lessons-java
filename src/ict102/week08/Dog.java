package ict102.week08;

public class Dog {
	// Fields
	private String size;
	public int age;
	public String color;

	// Methods
	public void speak() {
		System.out.println("Bark!");
	}

	public String getInfo() {
		return ("Size is " + size + " Age is " + age + " color is " + color);
	}

	public void setSize(String s) {
		size = s;
	}
}
