package ict102.week08;

public class DogRunner {
	public static void main(String[] args) {
		Dog charly = new Dog();
		charly.setSize("big");
		charly.age = 2;
		charly.color = "black";

		charly.speak();

		System.out.print(charly.getInfo());
	}
}
// output:
// Bark!
// Size is big Age is 2 color is black