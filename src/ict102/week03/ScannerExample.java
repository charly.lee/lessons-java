package ict102.week03;

import java.util.Scanner;

public class ScannerExample {

    public static void main(String[] args) {
        // Create a Scanner object
        Scanner scanner = new Scanner(System.in); // Read values from Console

        // A double value
        System.out.print("Enter Price: ");
        double price = scanner.nextDouble();

        // An int value
        System.out.print("Enter Quantity: ");
        int quantity = scanner.nextInt();

        // Display entered values
        double total = price * quantity;
        System.out.println();
        System.out.printf("%d @ %f = %f%n", quantity, price, total);

        scanner.close();
    }

}
