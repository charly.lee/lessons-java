package ict102.week11;

public class ExceptionArithmetic {
    public static void main(String[] args) {
        int denominator, numerator, ratio;
        numerator = 5;
        denominator = 0;

        try {
            ratio = numerator / denominator;
            System.out.println("ratio: " + ratio);
        } catch (ArithmeticException e) {
            System.out.println("Divide by 0.");
        }

        System.out.println("Done."); // Do not move this line
    }
}
