package ict102.week11;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Ex3 {

    public static void main(String[] args) {
        String filename = "./dist/no-such-file.txt";

        File file = new File(filename);

        System.out.println(file.canRead());

        BufferedReader br;

        try {
            br = new BufferedReader(new FileReader(filename));
            String line = "";
            while ((line = br.readLine()) != null) {
                System.out.println("Reading line: " + line);
            }
        } catch (FileNotFoundException e) {
            System.out.println("No file");
        } catch (IOException e) {
            System.out.println("Reader is not okay");
        }

    }
}
