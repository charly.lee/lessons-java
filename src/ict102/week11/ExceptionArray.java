package ict102.week11;

public class ExceptionArray {
    public static void main(String[] args) {
        int arr[] = { 1, 2, 3, 4, 5 };
        for (int i = 0; i <= arr.length; i++) { // java.lang.ArrayIndexOutOfBoundsException: 5
            System.out.println(arr[i]);
        }
    }
}
