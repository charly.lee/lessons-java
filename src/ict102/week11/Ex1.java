package ict102.week11;

public class Ex1 {

    public static void main(String[] args) {
        int denominator, numerator, ratio;
        
        numerator = 5;
        denominator = 0;
        
        try {
            ratio = numerator / denominator;
        } catch(java.lang.ArithmeticException e) {
            ratio = -1;
        }
        
        System.out.println("The answer is: " + ratio);
        System.out.println("Done."); // Don't move this line
    }
}

/*
1. 
The answer is: 2.5
Done.

2.
The answer is: Infinity
Done.

3.
The answer is: Infinity
Done.



*/