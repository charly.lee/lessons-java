package ict102.week11;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExceptionFile {
    public static void main(String[] args) {
        String filepath = "./dist/io/a.txt";

        BufferedReader br;

        try {
            br = new BufferedReader(new FileReader(filepath));
            String line;

            while ((line = br.readLine()) != null) {
                System.out.println("Reading line: " + line);
            }
        } catch (FileNotFoundException e) {
            System.out.println("file not found.");
        } catch (IOException e) {
            System.out.println("IOException occured.");
        }
    }
}
