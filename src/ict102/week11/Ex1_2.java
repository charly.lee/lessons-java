package ict102.week11;

public class Ex1_2 {

    public static void main(String[] args) {
        int denominator, numerator, ratio;
        numerator = 5;
        denominator = 0;
        
        try {
            ratio = numerator / denominator;
            System.out.println("The answer is: " + ratio);
        } catch (ArithmeticException ae) {
            System.out.println("Divide by 0.");
        }
        
        System.out.println("Done."); // Don't move this line
    }
}

// 6
// The code has no grammatical problem.
// It has been compiled.
