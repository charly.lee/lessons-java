package ict102.week11;

public class Ex2 {

    public static void main(String[] args) {
        int [] arr = {1, 3, 5, 7, 9};
        
        try {
            System.out.println(arr[0]);
            System.out.println(arr[arr.length -1]);
            System.out.println(arr[arr.length]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("ArrayIndexOutOfBoundsException!");
        }
    }
}
