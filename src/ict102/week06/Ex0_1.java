package ict102.week06;

public class Ex0_1 {
    public static void main(String [] args) {
        int [] arr1 = new int [3];  // define
        arr1[0] = 1;  // assign
        arr1[1] = 3; // assign
        arr1[2] = 5; // assign
        
        for(int i = 0; i < arr1.length; i++) {
            System.out.println("Item at index " 
                    + i + " " + arr1[i]);
        }
    }
}
