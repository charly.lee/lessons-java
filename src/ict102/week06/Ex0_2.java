package ict102.week06;

public class Ex0_2 {
    public static void main(String [] arr) {
        // define and assign
        int [] arr1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        
        for(int i = 0; i<arr1.length; i++) 
            System.out.println(i + ": " + arr1[i]);
        
        for(int n : arr1) {
            System.out.println(n);
        }
    }
}
