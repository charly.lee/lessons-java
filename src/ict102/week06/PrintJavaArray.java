package ict102.week06;

import java.util.Scanner;

public class PrintJavaArray {
	public static void main(String[] args) {
		char a = 'x';
		System.out.println(a);

		char[] arr = new char[3]; // define
		arr[0] = 'a'; // assign
		arr[1] = 'b'; // assign
		arr[2] = 'c'; // assign
		System.out.println(arr);
		System.out.println(arr.length);

		char[] arr2 = { 'a', 'b', 'c' };
		System.out.println(arr2);

		int[] arr3 = { 1, 2, 3, 4, 5 }; // define and assign
		System.out.println(arr3);

		String[] arr4 = { "one", "two", "three" };
		System.out.println(arr4);

		for (int i = 0; i < arr4.length; i++) {
			System.out.println(arr4[i]);
		}

		for (String item : arr4) {
			System.out.println(item);
		}

		// Exercise 1
		Scanner s = new Scanner(System.in);
		System.out.print("How many int numbers?: ");
		int size = s.nextInt();
		System.out.println("You entered: " + size);

		int[] arr5 = new int[size];

		for (int i = 0; i < arr5.length; i++) {
			System.out.print("Enter number: ");
			arr5[i] = s.nextInt();
		}

		s.close();

		System.out.println("Your numbers in forward order:");
		for (int n : arr5) {
			System.out.println(n);
		}

		System.out.println("Your numbers in backward order:");
		for (int i = arr5.length - 1; i >= 0; i--) {
			System.out.println(arr5[i]);
		}
	}
}
