package ict102.week06;

import java.util.Scanner;

public class Ex3 {
    public static void main(String [] args) {
        int [] arr = {11, 13, 15, 17, 1, 3, 5, 7, 9, 19, 21};
        
        Scanner s = new Scanner(System.in);
        
        System.out.print("Enter a number: ");
        int n = s.nextInt();
        
        for(int item : arr) {
            if(item < n) {
                System.out.print(item + ", ");
            }
        }
        
        System.out.println();
        
        for(int i=0; i<arr.length; i++) {
            if(arr[i] >= n) {
                System.out.print(arr[i] + ", ");
            }
        }
        
        System.out.println();
    }
}
