package ict102.week06;

public class Ex1 {
    public static void main(String [] args) {
        int [] ids = new int [4];  // define array
        double [] pays = new double [4]; // define array
        
        ids[0] = 1; // assign item
        pays[0] = 500.0; // assign item
        
        ids[1] = 2;
        pays[1] = 600.0;
        
        ids[2] = 3;
        pays[2] = 700.0;
        
        ids[3] = 4;
        pays[3] = 710.0;
        
        for(int i=0; i<ids.length; i++) {
            System.out.println(ids[i] + ": " + pays[i]);
        }
    }
}
