package ict102.week06;

public class Ex2 {
    public static void main(String [] args) {
        int [][] grades = {
            {13, 20, 40, 50, 60},
            {10, 20, 40, 50, 60},
            {55, 20, 44, 50, 60},
            {10, 24, 40, 50, 10},
            {65, 20, 45, 50, 60},
            {75, 20, 40, 50, 60},
            {86, 29, 40, 50, 100},
            {78, 20, 40, 50, 60},
            {10, 20, 87, 50, 60},
            {99, 20, 40, 66, 99}
        };
        
        int total = 0;
        int count = 0;
        
        for(int i = 0; i < grades.length; i++) {
            for(int j = 0; j < grades[i].length; j++) {
                total = total + grades[i][j];
                count++;
            }
        }
        
        System.out.println("total: " + total);
        System.out.println("count: " + count);
        System.out.println("avg: " + total / (float) count);
    }
}
