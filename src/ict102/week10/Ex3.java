package ict102.week10;

import java.io.FileWriter;
import java.io.IOException;

public class Ex3 {

    public static void main(String[] args) {
        int howManyDistanceInHour = 40;
        int howLongTravel = 3;

        for (int i = 1; i <= howLongTravel; i++) {
            System.out.println(i + " " + (i * howManyDistanceInHour));
        }

        String filename = "./dist/io/submit.10.3.txt";
        FileWriter fWriter;

        try {
            fWriter = new FileWriter(filename);

            for (int i = 1; i <= howLongTravel; i++) {
                fWriter.write(i + " " + (i * howManyDistanceInHour) + System.lineSeparator());
            }
            
            fWriter.close();

        } catch (IOException e) {
            System.out.println("File Input Output Exception.");
        }
    }   
}
