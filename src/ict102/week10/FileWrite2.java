package ict102.week10;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileWrite2 {
    public static void main(String[] args) throws IOException {
        appendText1();
        appendText2();

    }

    public static void appendText1() throws IOException {
        String in = "A small amount of text";
        FileWriter fWriter = new FileWriter("./dist/io/charDataSmall.3.txt", true);
        fWriter.write(in + System.lineSeparator());
        fWriter.close();
    }

    public static void appendText2() throws IOException {
        String in = "A small amount of text";
        FileWriter fWriter = new FileWriter("./dist/io/charDataSmall.3.txt", true);
        PrintWriter pWriter = new PrintWriter(fWriter);
        pWriter.println(in);
        pWriter.close();
        fWriter.close();
    }

}
