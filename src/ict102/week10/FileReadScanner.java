package ict102.week10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReadScanner {

    public static void main(String[] args) {
        File file = new File("./dist/io/scanner.1.txt");
        try {
            @SuppressWarnings("resource")
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
        }
    }
}
