package ict102.week10;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class LineNumbers {
	public static void main(String [] args) throws IOException {
		String filename = "./files/io/numbers.dat";
		
		File file = new File(filename);
		
		System.out.println(file);
		System.out.println(file.canRead());
		
		Scanner inFile = new Scanner(file);
		
		while (inFile.hasNextLine()) {
			System.out.println(inFile.nextInt());
		}
		
		inFile.close();
	}
}
