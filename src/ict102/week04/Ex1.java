package ict102.week04;

public class Ex1 {
	public static void main(String[] args) {
		// Convert a for loop to a while loop
		for (int x = 50; x > 0; x--) {
			System.out.println(x + " to go");
		}

		int h = 50;
		while (h > 0) {
			System.out.println(h + " to go");
			h -= 1;
		}
	}
}