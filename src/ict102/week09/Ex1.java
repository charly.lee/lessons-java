package ict102.week09;

public class Ex1 {
    public double plus() {
        return 0.1234;
    }
    
    public double plus(double d) {
        return d + d;
    }
    
    public double plus(double d1, double d2) {
        return d1 + d2;
    }
    
    public static void main(String [] args) {
        Ex1 obj = new Ex1();
        
        double x = obj.plus();
        double y = obj.plus(1.1);
        double z = obj.plus(2.2, 3.3);
        
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
    }
}
