package ict102.week09;

public class Dog {
    private String name;
    private int age;
    
    public Dog(String name) {
        this.name = name;
    }
    
    public Dog(String name, int age) {
        this.name = name;
        this.age = age;
    }
    
    public String toString() {
        return this.name + " is " + this.age + " years old.";
    }
    
    // Accessor
    public int getAge() {
        return this.age;
    }
    
    // Mutator
    public void setName(String newName) {
        this.name = newName;
    }
}
