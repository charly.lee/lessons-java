package ict102.week09;

public class DogMain {
    public static void main(String [] args) {
        Dog tommy = new Dog("Tommy"); // default contructor
        
        Dog charly = new Dog("Charly", 2);
        
        System.out.println(tommy.toString());
        System.out.println(charly.toString());
        
        System.out.println(tommy.getAge());
        
        tommy.setName("2nd-Tommy");
        System.out.println(tommy.toString());
    }
}
