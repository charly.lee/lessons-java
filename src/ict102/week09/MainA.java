package ict102.week09;

public class MainA {
    public static void main(String [] args) {
        Person p = new Person(); // default constructor.
        p.saySomething();
        
        Person p2 = new Person(34);
        p2.saySomething(5);
    }
}
