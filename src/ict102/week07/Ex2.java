package ict102.week07;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

/*
The ArrayList has 3 objects stored in it.
1: James
2: Catherine
3: Bill
*/
public class Ex2 {
    public static void main(String [] args) {
        int size = 3;
        List list = new ArrayList();
        
        Scanner s = new Scanner(System.in);
        
        for(int i=0; i<size; i++) {
            System.out.print("Enter Name " + (i+1) + ": ");
            list.add(s.nextLine());
        }
        
        System.out.println("The ArrayList has " + list.size() + " objects stored in it.");
        for(int i=0; i<list.size(); i++) {
            System.out.println((i+1) + ": " + list.get(i));
        }
        
        list.remove(1);
        
        System.out.println("The ArrayList has " + list.size() + " objects stored in it.");
        for(int i=0; i<list.size(); i++) {
            System.out.println((i+1) + ": " + list.get(i));
        }
        
        list.add(2, "Sam");
        list.add(3, "John");
        
        System.out.println("The ArrayList has " + list.size() + " objects stored in it.");
        for(int i=0; i<list.size(); i++) {
            System.out.println((i+1) + ": " + list.get(i));
        }
        
    }
}
