package ict102.week07;

import java.util.ArrayList;
import java.util.List;

public class Ex3 {
    public static void main(String [] strs) {
        double [] prices = {4.50, 10.50, 15.00};
        
        int [] item_numbers = {9, 6, 3};
        
        List<Double> list = new ArrayList<>();
        
        list.add(prices[0] * item_numbers[0]);
        list.add(prices[1] * item_numbers[1]);
        list.add(prices[2] * item_numbers[2]);
        
        double total = 0;
        
        for(Double item : list) {
            total += item;
        }
        
        System.out.println("Total : " + total);
    }
}
