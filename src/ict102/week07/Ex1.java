package ict102.week07;

import java.util.ArrayList;
import java.util.List;

public class Ex1 {

    public static void main(String[] args) {
        /*
        List listA = new ArrayList();
        listA.add(1);
        listA.add("A");
        
        int item0 = (int) listA.get(0);
        String item1 = (String) listA.get(1);
         */

        List<Integer> list = new ArrayList<>();  // JAVA Type Generics => for SAFE TYPE
        list.add(10);
        list.add(20);
        list.add(30);

        for (int i = 0; i < list.size(); i++) {
            int element = list.get(i);
            list.remove(i);
            list.add(0, element + 1);
        }

        System.out.println(list);
    }
}
