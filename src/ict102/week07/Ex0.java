package ict102.week07;

import java.util.ArrayList;
import java.util.List;

// differences between Array Type and List Type
public class Ex0 {
    public static void main(String [] args) {
        // Array Type
        int [] arr = {1,2,3,4,5};
        int theFirst = arr[0];
        int theLast = arr[arr.length - 1];
       
        // List Type
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        
        int theFisrtItem = list.get(0);
        int theLastItem = list.get(list.size() -1);
        
        
    }
}
