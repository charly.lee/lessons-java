package ict102.week07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrListEx1 {

    public static void main(String[] args) {
        List<Integer> arr = new ArrayList<>();
        arr.add(1);
        arr.add(2);
        arr.add(3);
        System.out.println(arr);

        List<Integer> arr2 = Arrays.asList(4, 5, 6);
        System.out.println(arr2);

        arr.addAll(arr2);
        Integer[] arr3 = arr.toArray(new Integer[arr.size()]);

        for (int i = 0; i < arr3.length; i++) {
            System.out.print(arr3[i]);
            if (i < arr3.length - 1) {
                System.out.print(" ,");
            } else {
                System.out.print("\n");
            }
        }
    }
}
