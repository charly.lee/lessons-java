package ict102.week07;

import java.util.ArrayList;

public class ArrListEx2 {
public static void main(String[] args) {
        ArrayList<String> arr4 = new ArrayList<>();
        arr4.add("A");
        arr4.add("B");
        arr4.add("C");
        System.out.println(arr4);
        
        arr4.remove(0);
        System.out.println(arr4);
        
        arr4.add(1, "D");
        System.out.println(arr4);
        
        arr4.add(0, "E");
        System.out.println(arr4);
    }
}
