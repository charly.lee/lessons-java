package ict102.week07;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ArrListEx3 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        List<Double> priceLst = new ArrayList<>();

        System.out.print("Number of items: ");
        int itemSize = s.nextInt();

        for (int i = 0; i < itemSize; i++) {
            System.out.print("Item " + (i + 1) + " Price: ");
            double price = s.nextDouble();
            System.out.print("Item " + (i + 1) + " count: ");
            double cnt = s.nextInt();
            priceLst.add(price * cnt);
        }

        System.out.println(priceLst);
        s.close();
    }
}
