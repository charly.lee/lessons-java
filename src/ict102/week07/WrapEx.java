package ict102.week07;

public class WrapEx {

    public static void main(String[] args) {
        int a = 1000;
        float b = 1.1f;
        double c = 2.2d;
        byte d = Byte.MAX_VALUE;
        short e = Short.MAX_VALUE;
        long f = Long.MAX_VALUE;

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
        System.out.println(e > d);
        System.out.println(f > e);
        System.out.println(Integer.MAX_VALUE);

        Number a1 = 1001;
        Number a2 = 11.1f;
        Number a3 = 50.5d;

        System.out.println(a1);
        System.out.println(a2);
        System.out.println(a3);

        Integer a4 = 1002;
        Float a5 = 11.2f;
        Double a6 = 50.5d;

        System.out.println(a1.floatValue());
        System.out.println(a2.intValue());
        System.out.println(a3.equals(a6));

        System.out.println(a4.compareTo(100));
        System.out.println(a5.isNaN());
        System.out.println(a6.isInfinite());
    }
}
