package ict102.week12;

public class Q7 {

    public static void main(String[] args) {
        int result, w = 5, x = 4, y = 8, z = 2;
        result = x + y;
        System.out.println(result);
        result = z * 2;
        System.out.println(result);
        result = y / x;
        System.out.println(result);
        result = y - z;
        System.out.println(result);
        result = w % 2; // modular operator
        System.out.println(result);
        
        System.out.println(5 / (float) 2);
    }

}
