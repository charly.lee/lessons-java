package ict102.week12;

public class Q8 {
    public static void main(String[] args) {
        String str1 = "Aa";
        String str2 = "Aaa";
        
        System.out.println(str1.equals(str2));
        System.out.println(str1 == str2);
        
        // Using '==' operator for comparing two instances in JAVA,
        // it will compare the memory addresses of them.
    }
}
