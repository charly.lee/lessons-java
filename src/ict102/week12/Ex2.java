package ict102.week12;

import java.io.*;
import java.util.*;

public class Ex2 {

    public static void main(String[] args) {
        
        /* This program contains errors! */
        
        try {
            File file = new File("MyFile.txt");
            Scanner inputFile = new Scanner(file);
            System.out.println(inputFile.nextLine());
        } catch (FileNotFoundException e) { 
            System.out.println("File not found."); 
        }
        
        
      

    }
}
