package ict102.week12;

public class Ex5 {

    public static void main(String[] args) {

       int [] numberArray1 = new int [100];
       int [] numberArray2 = new int [numberArray1.length];
       
       // Assign items numberArray1
       for (int i = 0; i< numberArray1.length; i++) {
           numberArray1[i] = (i+1);
       }
       
       // copy items numberArray1 - > numberArray2
       for(int i = 0; i< numberArray1.length; i++) {
           numberArray2[i] = numberArray1[i];
       }
       
    }
}
