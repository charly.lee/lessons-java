package ict102.week12;

public class Q6 {
    public static void main(String [] args) {
        int[] numberArray1 = new int[100];
        int[] numberArray2 = new int[100];

        // assign values of numberArray1's items
        for(int i = 0; i< numberArray1.length; i++) {
            numberArray1[i] = (i+1);
        }
        
        for(int i=0; i < numberArray1.length; i++) {
            System.out.print(numberArray1[i] + ", ");
        }
        
        // copy items from numberArray1 to numberArry2
        for(int i=0; i < numberArray2.length; i++) {
            numberArray2[i] = numberArray1[i];
        }
        
        System.out.println();
        
        for(int i=0; i < numberArray2.length; i++) {
            System.out.print(numberArray2[i] + ", ");
        }
    }
}
