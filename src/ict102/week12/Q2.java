package ict102.week12;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Q2 {

    public static void main(String[] args) {
        try {
            File file = new File("MyFile.txt");
            Scanner inputFile = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }

    }
}
