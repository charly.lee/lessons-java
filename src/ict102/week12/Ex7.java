package ict102.week12;

public class Ex7 {

    public static void main(String[] args) {
        int a = 0;
        int b  = 10;
        
       try {
           int c = 5/ 0;
       } catch (ArithmeticException e) {
           // gotcha!
       } finally {
           // this block always runs.
       }
      
       if(a < 0) {
           // do something here
       } else {
           // do something else here
       }
       
       if(a > 100) {
           
       } else if(a > 50) {
           
       } else {
           
       }
       
       
       do {
           b--;
       } while (b > 0);
       
       while (b < 10) {
           b++;
       }
       
       
    }
}
