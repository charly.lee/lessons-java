package ict102.week12;

public class Q5 {
    public static void main(String [] args) {
        int x;
        int y = 10;
        
        // if y = 10 then x = 100
        // else x = 1
        if(y == 10) {
            x = 100;
        } else {
            x = 1;
        }
        
        System.out.println(x);
        
    }
}
