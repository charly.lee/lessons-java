package ict102.week05;

public class ContainChars {

    public static int countContainOneOfChars(String s, char[] cs) {
        if (s == null || cs.length < 1) {
            return 0;
        }

        int r = 0;
        for (int i = 0; i < s.length(); i++) {
            for (int j = 0; j < cs.length; j++) {
                if (s.charAt(i) == cs[j]) {
                    r++;
                }
            }
        }

        return r;
    }

    public static void main(String[] args) {
        // ASCII Code (https://www.ascii-code.com/)

        String str = "fj3k5k2h2  	rkrkkr3";

        char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        char[] digitsAsciiCode = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59};

        char[] spaces = {' ', '	'};
        char[] spacesAsciiCode = {9, 32};

        System.out.println(countContainOneOfChars(str, digits));
        System.out.println(countContainOneOfChars(str, digitsAsciiCode));
        System.out.println(countContainOneOfChars(str, spaces));
        System.out.println(countContainOneOfChars(str, spacesAsciiCode));
    }

}
