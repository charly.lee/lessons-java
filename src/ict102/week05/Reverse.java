package ict102.week05;

// import java.util.Scanner;

public class Reverse {
    public static void main(String [] args) {
        // Scanner s = new Scanner(System.in);
        // String inStr = s.next();
        String inStr = "abcdefg";
        int inStrLen = inStr.length();
        
        char[] chs = new char[inStrLen];
        
        for(int idx = inStrLen - 1, i = 0; idx >= 0; idx--, i++) {
            chs[i] = inStr.charAt(idx);
        }
        
        System.out.println("The reverse of the String \"" + inStr + "\" is \"" + String.valueOf(chs) + "\".");
        
    }
}
