package examples.run;

/**
 * Java Hello World
 * 
 * <pre>
 * javac -cp ./build/classes -d ./build/classes ./src/examples/run/ExampleApp1.java
 * java -cp ./build/classes examples.run.ExampleApp1
 * </pre>
 */
public class ExampleApp1 {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

}
