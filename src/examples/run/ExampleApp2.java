package examples.run;

/**
 * Logging process arguments
 *
 * <pre>
 * javac -cp ./build/classes -d ./build/classes ./src/examples/run/ExampleApp2.java
 * java -cp ./build/classes examples.run.ExampleApp2 arg1 arg2 arg3
 * </pre>
 */
public class ExampleApp2 {

    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            System.out.println(args[i]);
        }

        for (String s : args) {
            System.out.println(s);
        }

    }

}
