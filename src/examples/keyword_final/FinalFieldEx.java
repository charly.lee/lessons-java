package examples.keyword_final;

public class FinalFieldEx {

    public static final String SCHOOL_NAME = "SCHOOL_A";

    public static void main(String[] args) {
        // cannot assign a value to final variable SCHOOL_NAME
        // SCHOOL_NAME = "SCHOOL_B";
    }
}
