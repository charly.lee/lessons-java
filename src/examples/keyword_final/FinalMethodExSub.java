package examples.keyword_final;

public class FinalMethodExSub extends FinalMethodEx {
    // method1() in FinalMethodExSub cannot override method1() in FinalMethodEx
    // overridden method is final
    /*
     * public final int method1() { return 2; }
     */

    public int method2() {
        return 1;
    }
}
