package examples.keyword_final;

import java.util.ArrayList;
import java.util.List;

public class FinalParameterEx {

    public static int method1(final int size) {
        List<String> list_1 = new ArrayList<>();
        list_1.add("1");

        // final parameter size may not be assigned
        // size = 4;
        return list_1.size() + size;
    }

    public static int method2(final List<String> list) {
        List<String> list_1 = new ArrayList<>();
        list_1.add("1");

        // final parameter list may not be assigned
        // list = new ArrayList();
        // final parameter object
        list.add("New Item");

        return list_1.size() + list.size();
    }

    public static void main(String[] args) {
        List<String> l = new ArrayList<>();
        l.add("1");
        l.add("2");

        System.out.println(method1(3));
        System.out.println(method2(l));
    }
}
