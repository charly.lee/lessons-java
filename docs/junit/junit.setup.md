# JUnit

## Install

### Download

Download libs : https://github.com/junit-team/junit4/wiki/Download-and-Install

Locate jar files in your `classpath`.

### Maven

```xml
<dependency>
  <groupId>junit</groupId>
  <artifactId>junit</artifactId>
  <version>4.12</version>
</dependency>
```

### Gradle

```gradle
apply plugin: 'java'

dependencies {
  testCompile 'junit:junit:4.12'
}
```

### Netbeans

```
Project - Properties - Libraries - Compile Tests - Add Library - junit

Project - Properties - Libraries - Compile Tests - Add Library - hamcrest
```

![JUint, Netbeans Lib](https://loggar.github.io/note/settings/junit.netbeans.lib.PNG)
