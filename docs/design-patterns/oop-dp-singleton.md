# Singleton Pattern

## Singleton:

- A class that is instantiated exactly once.
- Exactly one instance is needed to coordinate actions across the system.

## Why Singleton:

- Object Instantiation is expensive (most expensive operation)
- Memory space is occupied for every instance.
- Repeated Instantiations lead to repeated memory-deallocations.

## Singleton is a `Stateless` or `global-state` class.

Stateless Objects

- Have no mutable field.
- Actions always isolated from its fields or other actions.

```java
/**
 * Simple example of stateless object.
 *
 */
public class StatelessObject {
    public String capitalizer(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}


public class StatelessObjectTest {
    /**
     * Stateless Object: No matter how many instances exist, the results of action are identical.
     *
     */
    @Test
    public void test() {
        StatelessObject i1 = new StatelessObject();
        StatelessObject i2 = new StatelessObject();

        assertTrue(i1 != i2);
        assertTrue(i1.capitalizer("student1").equals(i2.capitalizer("student1")));
    }
}
```

## Implementation

![Singleton, class diagram](https://loggar.github.io/note/diagrams/diagram.singleton.a1.PNG)

### 1. Using Pre-instantiation.

```java
public class SingletonEx1 {
    private static final SingletonEx1 INSTANCE = new SingletonEx1();

    /**
     * This cannot be instantiated from the others.
     */
    private SingletonEx1() {
    }

    public static SingletonEx1 getInstance() {
        return INSTANCE;
    }

    public String capitalizer(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}

public class SingletonEx1Test {
    @Test
    public void singletonInstanceTest() {
        // SingletonEx1 i0 = new SingletonEx1();
        // The constructor SingletonEx1() is not visible

        SingletonEx1 i1 = SingletonEx1.getInstance();
        SingletonEx1 i2 = SingletonEx1.getInstance();

        assertTrue(i1 == i2);
        assertTrue(i1.capitalizer("student1").equals(i2.capitalizer("student1")));
    }
}
```

### 2. Using enum

```java
public enum SingletonEx2 {
    INSTANCE;

    /**
     * If you prefer to follow the class diagram conventions:
     */
    public static SingletonEx2 getInstance() {
        return INSTANCE;
    }

    public String capitalizer(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}

public class SingletonEx2Test {
    @Test
    public void singletonInstanceTest() {
        SingletonEx2 i3 = SingletonEx2.INSTANCE;
        SingletonEx2 i4 = SingletonEx2.INSTANCE;

        SingletonEx2 i1 = SingletonEx2.getInstance();
        SingletonEx2 i2 = SingletonEx2.getInstance();

        assertTrue(i3 == i4);
        assertTrue(i3.capitalizer("student1").equals(i4.capitalizer("student1")));

        assertTrue(i1 == i2);
        assertTrue(i1.capitalizer("student1").equals(i2.capitalizer("student1")));
    }
}
```

## Next

- Thread-Safe Singletons.
- Static Utility Classes (no instantiation, stateless)
